import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    @Test
    public void checkCircleIsInitialisedWithRadius7() {
        Circle circle = new Circle(7);
        assertEquals(new Circle(7), circle);
    }

    @Test
    public void checkCircleIsInitialisedWithRadius5() {
        Circle circle = new Circle(5);
        assertEquals(new Circle(5), circle);
    }

    @Test
    public void shouldReturnAreaRadius5() {
        Circle circle = new Circle(5);
        double dArea = 3.14 * 5 * 5;

        assertEquals(dArea, circle.area(), 0.001);
    }

    @Test
    public void shouldReturnAreaRadius7() {
        Circle circle = new Circle(7);
        double dArea = 3.14 * 7 * 7;

        assertEquals(dArea, circle.area(), 0.001);
    }

    @Test
    public void shouldReturnPerimeterRadius7(){
        Circle circle = new Circle(7);
        double dPerimeter = 2*3.14 * 7 ;

        assertEquals(dPerimeter, circle.perimeter(), 0.001);
    }

    @Test
    public void shouldReturnPerimeterRadius5(){
        Circle circle = new Circle(5);
        double dPerimeter = 2*3.14 * 5 ;

        assertEquals(dPerimeter, circle.perimeter(), 0.001);
    }


}
