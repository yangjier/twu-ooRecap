import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

    Rectangle rectangle = new Rectangle(3,4);

    @Test
    public void shouldInitRectangle(){
        assertEquals(rectangle,new Rectangle(3,4));
    }

    @Test
    public void shouldInitSquare(){
        Rectangle rectangleSquare = new Rectangle(5,5);
        assertEquals(rectangleSquare,Rectangle.square(5));
    }

    @Test
    public void shouldReturnSquareArea(){
        assertEquals(25,Rectangle.square(5).area(),0.001);
    }

    @Test
    public void shouldReturnRectangleArea(){
        assertEquals(rectangle.area(),12.0,0.001);
    }

    @Test
    public void shouldReturnSquarePerimeter(){
        assertEquals(20,Rectangle.square(5).perimeter(),0.001);
    }

    @Test
    public void shouldReturnRectanglePerimeter(){
        assertEquals(rectangle.perimeter(),14,0.001);
    }
}

