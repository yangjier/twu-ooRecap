public class Circle  implements Shape{

    private final int radius;

    private final double PI = 3.14;

    public Circle(int radius){
        this.radius=radius;
    }

    @Override
    public double area() {
        return PI*radius*radius;
    }

    @Override
    public double perimeter() {
        return 2*PI*radius;
    }
    @Override
    public boolean equals(Object circle) {
        Circle anotherCircle = (Circle) circle;
        return this.radius==anotherCircle.radius;
    }
}
