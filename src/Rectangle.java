public class Rectangle implements Shape {
    private final int length;
    private final int breadth;

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }

    public double area() {
        return length * breadth;
    }

    public double perimeter() {
        return 2 * (length + breadth);
    }

    static Rectangle square(int side) {
        return new Rectangle(side, side);
    }

    @Override
    public boolean equals(Object rectangle) {
        Rectangle anotherRectangle = (Rectangle) rectangle;
        return this.breadth == anotherRectangle.breadth && this.length == anotherRectangle.length;
    }
}